import pygame
import random
import sys
import time
import pygame.display

pygame.init()


#Adding variables such as Width, length, colours ECT.

width = 600
height = 600
WHITE = (255,255,255)
BLACK = (0,0,0)
RED = (255,0,0)
BLUE = (0,0,255)

#Setting the screen settings here to be able to actual open a window.
screen = pygame.display.set_mode((width,height))
pygame.display.set_caption("ASTEROIDS")
clock = pygame.time.Clock()

#Adding the pictures which in this case the rocket and the asteroid
rocket_width = 175
rocket_length = 175
Rocket = pygame.image.load('Spaceman.png')
Rocket = pygame.transform.scale(Rocket,(rocket_width, rocket_length))

asteroid_width = 150
asteroid_height = 150
asteroid = pygame.image.load('Asteroid3.png')
asteroid = pygame.transform.scale(asteroid,(asteroid_width, asteroid_height))

bkgd = pygame.image.load('Background.png').convert()
bkgd = pygame.transform.scale(bkgd,(600, 600))



def asteroids_dodged(count):
	font = pygame.font.SysFont(None, 25)
	text = font.render("Dodged: "+str(count), True, WHITE)
	screen.blit(text, (0,0))


def asteroids(ax, ay, aw, ah):
	screen.blit(asteroid,(ax,ay))

def rocket(x,y):
	screen.blit(Rocket,(x,y))

def text_object(text, font):
	textsurface = font.render(text, True, RED)
	return textsurface, textsurface.get_rect()

def draw_txt(text):
	fontnamesize = pygame.font.Font('freesansbold.ttf',30)
	TextSurf, TextRect = text_object(text, fontnamesize)
	TextRect.center = ((width / 2) , (height / 2))
	screen.blit(TextSurf, TextRect)


	pygame.display.update()
	time.sleep(2)
	game()

def crash():
	draw_txt('You Crashed into an asteroid!')


def game():
#Defining x & y for where the rocket is on the screen.
	x = (width * 0.4)
	y = (height * 0.6)
	bkgdy = 0
	a_startx = random.randrange(0, width)
	a_starty = -600
	aspeed = 1

	xmove = 0

	dodged = 0



	exit = False

	while not exit:

		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				quit()
#Adding movement to the rocket. When the right arrow is pressed, x coordinate will increase by 5 make the rocket move and when the left arrow is pressed, the x coordinate will decrease by 5 making the rocket move left.

			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT:
					xmove = -5
				elif event.key == pygame.K_RIGHT:
					xmove = 5

			if event.type == pygame.KEYUP:
				if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
					xmove = 0
		
		x += xmove
		rel_y = bkgdy % bkgd.get_rect().height
		screen.blit(bkgd, (0, rel_y - bkgd.get_rect().height))
		if rel_y < 600:
			screen.blit(bkgd, (0, rel_y))
		bkgdy -= -3




		asteroids(a_startx, a_starty, asteroid_width, asteroid_height)
		a_starty += aspeed
		rocket(x,y)
		asteroids_dodged(dodged)
#This is adding the boundary to the game to ensure the player cannot go off the screen.
		if x > width - rocket_width or x < 0:
			crash()

		if a_starty > height:
			a_starty = 0 - asteroid_height
			a_startx = random.randrange(0,width)
			dodged += 1
			aspeed += 0.5

		if y < a_starty+asteroid_height:
			print('y crossover')


			if x > a_startx and x < a_startx+asteroid_width or x+rocket_width > a_startx and x + rocket_width < a_startx + asteroid_width:
				print('x crossover')
				crash()




		pygame.display.update()
		clock.tick(60)
game()
pygame.quit()
quit()